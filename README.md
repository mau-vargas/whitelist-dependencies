# Whitelist Dependencies
**whitelist dependencies** es un yml de gitlab que nos ayuda a validar todas las dependencias que contiene una aplicación, 
solo es exitoso cuando las librerías que ocupa la app están en el [**whitelist.txt**](https://gitlab.falabella.com/fif/canales-digitales/mobile/continuous-delivery/android/commons/whitelist-dependencies/-/blob/main/whitelist.txt),de lo contrario es necesario mandar MR a los moderadores de la comunidad para actualizar las dependencias .

Requisitos para agregar un módulo al **whitelist**:
- Readme
- Pruebas unitarias 80% de cobertura
- Agregar modulo al espacio de confluence [link](https://confluence.falabella.com/display/CAT2/Modulos+Android)
- Especificar uso de la nueva dependencia 
- No puede ser una librería de github
