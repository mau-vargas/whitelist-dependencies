#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[1;33m"

#exit 1
#obtener todas las dependencias 
./gradlew -q :app:androidDependencies > dependencies.txt

#obtener lista blanca
#git clone https://gitlab.falabella.com/fif/canales-digitales/mobile/android/commons/whitelist-dependencies.git
#cd whitelist-dependencies
#git checkout master
#cd ..



validateDependencies () {
 grep -w $1 whitelist-dependencies/whitelist.txt > dependencies2.txt
 var6="$(wc -l dependencies2.txt | cut -f1 -d '.' | sed 's/^[[:space:]]*//')"
  if [ "$var6" = "0 dependencies2" ]; then
    echo "$YELLOW Actualizar librería $1"
  else
   echo "$GREEN Librería actualizada  $1"  
  fi
}

cleanDependencie ()  {
  replace=""
  VAR1=$(echo $1 | sed -e "s/@aar/$replace/g")
  VAR2=$(echo $VAR1 | sed -e "s/+--- /$replace/g")
  VAR3=$(echo $VAR2 | sed -e "s/\/--- /$replace/g")
  VAR4=$(echo $VAR3 | sed -e "s/\\--- /$replace/g")
  VAR5=$(echo $VAR4 | sed -e "s/@jar/$replace/g")
  validateDependencies "$VAR5"
}


#Recorrer lista balnca
while IFS= read -r line
do

string='@'

 # if [ -z "${aar##*$line*}" ] || [ -z "${jar##*$line*}" ]   ; then
if [ -z "${line##*$string*}" ] ;then
    cleanDependencie "$line"
fi   
 
done < dependencies.txt



rm -rf dependencies.txt
rm -rf dependencies2.txt
rm -rf whitelist-dependencies


